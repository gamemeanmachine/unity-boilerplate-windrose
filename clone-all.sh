#!/bin/bash
git clone --branch 0.0.2 git@github.com:AlephVault/unity-support.git  ../unity-support
git clone --branch 0.0.1 git@github.com:AlephVault/unity-support-generic.git ../unity-support-generic
git clone --branch 0.0.1 git@github.com:AlephVault/unity-boilerplates.git ../unity-boilerplates
git clone --branch 0.0.1 git@github.com:AlephVault/unity-layout.git ../unity-layout
git clone --branch 0.0.1 git@github.com:AlephVault/unity-menu-actions.git ../unity-menu-actions
git clone --branch 0.0.1 git@github.com:AlephVault/unity-soundaround.git ../unity-soundaround
git clone --branch 0.0.4 git@github.com:AlephVault/unity-spriteutils.git ../unity-spriteutils
git clone --branch 0.0.1 git@github.com:AlephVault/unity-textureutils.git ../unity-textureutils
git clone --branch 0.0.1 git@gitlab.com:gamemeanmachine/unity-backpack.git ../unity-backpack
git clone --branch 0.0.1 git@gitlab.com:gamemeanmachine/unity-gabtab.git ../unity-gabtab
git clone --branch 0.0.6 git@gitlab.com:gamemeanmachine/unity-windrose.git ../unity-windrose
git clone --branch 0.0.4 git@gitlab.com:gamemeanmachine/unity-windrose-biomes.git ../unity-windrose-biomes
git clone --branch 0.0.1 git@gitlab.com:gamemeanmachine/unity-windrose-backpack-plugin.git ../unity-windrose-backpack-plugin
git clone --branch 0.0.1 git@gitlab.com:gamemeanmachine/unity-windrose-gabtab-plugin.git ../unity-windrose-gabtab-plugin
git clone --branch 0.0.4 git@gitlab.com:gamemeanmachine/unity-windrose-spriteutils.git ../unity-windrose-spriteutils
git clone --branch 0.0.3 git@gitlab.com:gamemeanmachine/unity-windrose-neighbourteleports.git ../unity-windrose-neighbourteleports
git clone --branch 0.0.1 git@gitlab.com:gamemeanmachine/unity-windrose-cubeworlds.git ../unity-windrose-cubeworlds
git clone --branch 0.0.4 git@gitlab.com:gamemeanmachine/unity-windrose-refmapchars.git ../unity-windrose-refmapchars
git clone --branch 0.0.1 git@gitlab.com:gamemeanmachine/unity-windrose-lpcbiomes.git ../unity-windrose-lpcbiomes

